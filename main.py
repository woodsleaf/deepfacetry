#!pip install deepface
from deepface import DeepFace
models = ["VGG-Face", "Facenet", "Facenet512", "OpenFace", "DeepFace", "DeepID", "ArcFace", "Dlib", "Human-beings"] # 7,8 not work
metrics = ["cosine", "euclidean", "euclidean_l2"]
backends = ['opencv', 'ssd', 'dlib', 'mtcnn', 'retinaface', 'mediapipe']
# verify, find, analyze, stream, detectFace

#Это для работы с вебкамерой
def check_webcam_stream(db = './user/database', num_model = 0, measure = 0, id_detectback = 0):
    DeepFace.stream(db_path = db, model_name = models[num_model], distance_metric = metrics[measure], detector_backend = backends[id_detectback]) #[ WARN:0@15.125] global /io/opencv/modules/videoio/src/cap_v4l.cpp (1000) tryIoctl VIDEOIO(V4L2:/dev/video0): select() timeout.

#check_webcam_stream()

#Verify eq between two pictures
def check_eq(img1 = 'img1.jpg', img2 = 'img2.jpg', num_model = 0, measure = 0, id_detectback = 0):
    result = DeepFace.verify(img1_path = img1, img2_path = img2, model_name = models[num_model], distance_metric = metrics[measure], detector_backend = backends[id_detectback])
    #result = DeepFace.verify(img1, img2, model_name = models[num_model])
    print(result)
    return result

#True
# check_eq('img1.jpg', 'img2.jpg')
# check_eq('img1.jpg', 'img2.jpg', 1)
#False
# check_eq('img1.jpg', 'olga.jpg')


#Find close img in directory
def check_eq_multy(img = 'img1.jpg', catalog = './catalog', num_model = 0, measure = 0, id_detectback = 0):
    df = DeepFace.find(img_path = img, db_path = catalog, model_name = models[num_model], distance_metric = metrics[measure], detector_backend = backends[id_detectback])
    # df = DeepFace.find(img, catalog, model_name = models[num_model])
    print(df)
    return df

# check_eq_multy('img1.jpg', 'catalog')
# check_eq_multy('img1.jpg', 'catalog', 0, 0)

#Analyze check attribute face
def check_emoteagr(img = 'img1.jpg', id_detectback = 0):
    obj = DeepFace.analyze(img_path = img, actions = ['age', 'gender', 'race', 'emotion'], detector_backend = backends[id_detectback])
    print(obj)
    return obj

#check_emoteagr('img1.jpg')

#detectFace and alignment # не понял как работает эта матрица
def detection_face(img = 'img1.jpg', size = (224,224), id_detectback = 0):
    face = DeepFace.detectFace(img_path = img, target_size = size, detector_backend = backends[id_detectback])
    #print(size)
    print(face)
    return face

#detection_face('img1.jpg')

#Represent
def representface(img = 'img1.jpg', id_model = 1):
    embedding = DeepFace.represent(img_path = img, model_name = models[id_model])
    print(embedding)
    return embedding

#representface()