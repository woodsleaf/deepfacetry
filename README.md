# Source manual

https://pypi.org/project/deepface/

## Prepare

mkdir ~/dface
python3 -m venv ~/dface/.venv
source ~/dface/.venv/bin/activate
cd ~/dface
pip install deepface

## Execute

python3 main.py

## Example API

python3 api.py

## deactivate venv if needed

deactivate

